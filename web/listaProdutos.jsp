<%-- 
    Document   : listaProdutos
    Created on : 05/06/2016, 22:21:52
    Author     : Laut
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Listagem de Produtos</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" >  
    </head>
    <body>
        <div class="container">
            <h1>Produtos</h1>
            <hr>
            <table class="table table-hover">
                <tr class="danger">
                    <th>Código</th>
                    <th>Descrição</th>
                    <th>Preço</th>
                    <th>Validade</th>
                </tr>
                <c:forEach var="produto" items="${lprodutos}">
                    <tr>
                        <td> <c:out value= "${produto.codigo}"/> </td>
                        <td> <c:out value="${produto.descricao}"/> </td>
                        <td> <fmt:formatNumber type="currency" value="${produto.preco}"/></td>
                        <td> <fmt:formatDate pattern="MM/yyyy" value="${produto.dataValidade}"/> </td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </body>
</html>
