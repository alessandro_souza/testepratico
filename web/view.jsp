<%-- 
    Document   : view
    Created on : 05/06/2016, 21:50:21
    Author     : Laut
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" >  

    </head>
    <body>
        <div class="container">
            <h1>Área da Figura</h1>

            <hr>
            <h4 class="">
                Valor: 
                <c:if test="${param.figura eq 'Quadrilátero'}">
                    <c:out value="${param.base * param.altura}"/> m2
                </c:if>
                <c:if test="${param.figura eq 'Triângulo'}">
                    <c:out value="${(param.base * param.altura)/2}"/> m2
                </c:if> 
            </h4>
        </div>
    </body>
</html>
